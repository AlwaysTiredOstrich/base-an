import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {Feature1Component} from "../../components/feature1/feature1.component";
import {Feature2Component} from "../../components/feature2/feature2.component";
import {Feature3Component} from "../../components/feature3/feature3.component";
import {BaseFeaturesParentComponent} from "../../components/base-features-parent/base-features-parent.component";

@NgModule({
  declarations: [Feature1Component, Feature2Component, Feature3Component, BaseFeaturesParentComponent],
  imports: [
    CommonModule
  ],
  exports: [
    Feature1Component, Feature2Component, Feature3Component, BaseFeaturesParentComponent
  ]
})
export class BaseANModule { }
