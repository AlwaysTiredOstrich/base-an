import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFeaturesParentComponent } from './base-features-parent.component';

describe('BaseFeaturesParentComponent', () => {
  let component: BaseFeaturesParentComponent;
  let fixture: ComponentFixture<BaseFeaturesParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseFeaturesParentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BaseFeaturesParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
