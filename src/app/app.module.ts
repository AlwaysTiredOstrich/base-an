import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {BaseANModule} from "./modules/base-an/base-an.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BaseANModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
